#![allow(unused)]

use std::collections::*;

use reddl_grid::space::*;
use reddl_grid::tile::*;

use crate::*;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct SourcePoint {
    /// The position (in route space) of this source point.
    pos: Pos,

    /// How much signal power there would be at this point.
    signal: u8,
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct DestPoint {
    /// The face position position for this destination point.  (Y, Z)
    pos: (i32, i32),

    /// The minimum signal expected at this point, could be 1 and be ok, if it's
    /// 0 then it would probably break things.
    min_signal: u8,
}

impl DestPoint {
    pub fn is_pos_in_line(&self, Pos(_, y, z): Pos) -> bool {
        self.pos.0 == y && self.pos.1 == z
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
struct RouteTip {
    pos: Pos,
    dir: Cardinal,
    signal: u8,
}

impl RouteTip {
    pub fn to_output_pos(&self, o: PieceOutput) -> RouteTip {
        RouteTip {
            pos: self.pos + o.pos.rotate_n_around_origin(self.dir.ordinal()),
            dir: Cardinal::from_ordinal(self.dir.ordinal() + o.dir.ordinal()),
            signal: match o.signal {
                SignalRule::Absolute(s) => s,
                SignalRule::Relative(d) => (self.signal as i8 + d) as u8,
            },
        }
    }

    pub fn is_forward(&self) -> bool {
        self.dir == Cardinal::East
    }
}

impl From<SourcePoint> for RouteTip {
    fn from(f: SourcePoint) -> Self {
        RouteTip {
            pos: f.pos,
            dir: Cardinal::East,
            signal: f.signal,
        }
    }
}

/// A link plan is the overall set of needed links between two steps in an
/// interface between a source and destination.
#[derive(Clone, Hash, Debug)]
pub struct LinkPlan {
    link_sets: Vec<LinkSet>,
}

/// A link set is a set of links that should have similar-looking paths in a
/// path plan.  Like two busses that should turn at similar points.
#[derive(Clone, Hash, Debug)]
pub struct LinkSet {
    links: Vec<(SourcePoint, DestPoint)>,
}

/// A route path is a set of steps of (possibly repeated) pieces.
#[derive(Clone, Hash, Debug)]
pub struct RoutePath {
    steps: Vec<PathStep>,
}

impl RoutePath {
    pub fn get_total_delay(&self) -> u32 {
        (self.steps.iter())
            .map(|p| p.piece.output.delay as u32 * p.repetition as u32)
            .sum()
    }

    pub fn get_final_signal(&self, start: u8) -> u8 {
        let mut signal = start;
        for s in &self.steps {
            for _ in 0..s.repetition {
                signal = s.piece.get_expected_signal(signal);
            }
        }
        signal
    }

    pub fn to_pieces(&self) -> Vec<&RoutePiece> {
        let mut pieces = Vec::new();
        for s in &self.steps {
            for _ in 0..s.repetition {
                pieces.push(&s.piece);
            }
        }
        pieces
    }
}

/// Path steps are sequences of repeated route pieces, like repeated single
/// steps in a path for going a longer distance.
#[derive(Clone, Hash, Debug)]
pub struct PathStep {
    piece: RoutePiece,
    repetition: u16,
}

/// This is the solved solution for a link plan, with all links pathed out
/// including how far apart the source and destination should have to be.
#[derive(Clone, Hash, Debug)]
pub struct LinkSolution {
    link_paths: Vec<(SourcePoint, RoutePath)>,
    separation: u32,
}

#[derive(Debug)]
pub enum Error {
    Other(String),
}

/// This function is where all the magic logic happens.
pub fn calculate_paths(plan: &LinkPlan, dict: &Vec<RoutePiece>) -> Result<LinkSolution, Error> {
    unimplemented!()
}

/// Attempts to solve a single link path, with the given paremeters.
fn solve_single_link(
    src: &SourcePoint,
    dest: &DestPoint,
    dict: &Vec<RoutePiece>,
    set: &LinkSet,
    world: &Space<Tile>,
) -> Result<RoutePath, Error> {
    unimplemented!()
}

/// Tries to extend the path forward in case we need to make it longer for
/// some reason, placing repeaters as necessary.
fn extend_path_forward(
    src: &SourcePoint,
    current_path: &RoutePath,
    target_x: i32,
    set: &LinkSet,
    world: &Space<Tile>,
) -> Result<RoutePath, Error> {
    unimplemented!()
}

#[derive(Clone, Debug)]
enum NextStepOptions {
    /// Start a new step with a new piece.
    NewPiece(RoutePiece),

    /// Extend this peice one more time, like for a straight stretch.
    ExtendSteps,
}

/// Calculates all the potential options we have for picking our next step, in
/// order of what should be checked first.
fn compute_options(
    world: &Space<Tile>,
    dict: &Vec<RoutePiece>,
    dest: DestPoint,
    tip: RouteTip,
    prev_step: Option<&PathStep>,
) -> Vec<NextStepOptions> {
    let mut opts = Vec::new();

    // A simple logic check to see if continuing as-is is useful.
    if prev_step.is_some() {
        let ps = prev_step.unwrap();
        if ps.piece.is_straight() && tip.is_forward() && dest.is_pos_in_line(tip.pos) {
            opts.push(NextStepOptions::ExtendSteps);
        }
    }

    // Now add all the pieces that can be placed down.
    for p in dict {
        if prev_step.is_some() {
            if prev_step.unwrap().piece == *p {
                // This would just be repeating what we already did.
                continue;
            }
        }

        if check_piece_placable(&world, &p) {
            opts.push(NextStepOptions::NewPiece(p.clone()));
        }
    }

    // Return
    opts
}

/// Verifies that the peice can be placed in the world, making sure we don't
/// override any tiles with it and that the pieces masks don't conflict with any
/// tile that are already there.
fn check_piece_placable(world: &Space<Tile>, piece: &RoutePiece) -> bool {
    unimplemented!()
}

#[derive(Clone, Hash, Debug)]
enum TipComparison {
    A,
    B,
    Equal,
}

/// Determines which tip is the "better" one, given the destination and the tip.
fn compare_tips(prev: RouteTip, dest: DestPoint, a: RouteTip, b: RouteTip) -> TipComparison {
    unimplemented!()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_route_tip_forward_10() {
        // like a long string that moves forwards 10 blocks
        let out = PieceOutput {
            pos: Pos(10, 0, 0),
            dir: Cardinal::East,
            signal: SignalRule::Relative(-10),
            delay: 0,
        };

        let tip = RouteTip {
            pos: Pos(0, 0, 0),
            dir: Cardinal::East,
            signal: 15,
        };

        let expected = RouteTip {
            pos: Pos(10, 0, 0),
            dir: Cardinal::East,
            signal: 5,
        };

        assert_eq!(tip.to_output_pos(out), expected);
    }

    #[test]
    fn test_route_tip_turn_left() {
        // like a left turn
        let out = PieceOutput {
            pos: Pos(1, 0, -2),
            dir: Cardinal::North,
            signal: SignalRule::Relative(-3),
            delay: 0,
        };

        let tip = RouteTip {
            pos: Pos(10, 20, 30),
            dir: Cardinal::West,
            signal: 20,
        };

        let expected = RouteTip {
            pos: Pos(9, 20, 32),
            dir: Cardinal::South,
            signal: 17,
        };

        assert_eq!(tip.to_output_pos(out), expected);
    }

}
