extern crate reddl_grid;

//use std::collections::*;

use reddl_grid::tile::*;
use reddl_grid::*;

mod pather;
mod squarenet;

/// A route piece is a template for extending the path from a source tile to a
/// destination position when connection two source/destination panes.
///
/// The logic assumes that (0, 0, 0) is where the signal is being provided from
/// a source or the route piece directly before it in the path.
///
/// This also uses x as the "forward" direction, and z/y is the right/up of
/// course.  So x is to the east.  Global rotations are computer later.
#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct RoutePiece {
    /// Which tiles to put at which relative positions.
    pub tiles: Vec<(Pos, Tile)>,

    /// Position that the signal will end up after this piece, and what the
    /// signal should be at that position.  The next tile will assume (0, 0, 0)
    /// are one of these values, ideally should be directly powered.
    pub output: PieceOutput,

    /// There should not be any wire in any of these locations before we apply
    /// this piece, so as to avoid invalid connections.
    pub wire_mask: Vec<Pos>,

    /// There should not be any solids in these positions before we apply this
    /// piece, so as to avoid invalid connections or accidental misconnection.
    pub solid_mask: Vec<Pos>,
}

/// The first value is the pos, third is the signal propagation time, fourth
/// is delay, in case of repeaters or torches.  Remember each repeater step
/// counts as 2 delay units.
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct PieceOutput {
    pub pos: Pos,
    pub dir: Cardinal,
    pub signal: SignalRule,
    pub delay: u8,
}

impl RoutePiece {
    pub fn rotate_ccw(&self) -> RoutePiece {
        RoutePiece {
            tiles: (self.tiles.iter())
                .map(|(Pos(x, y, z), t)| (Pos(z * -1, *y, *x), t.rotate_ccw()))
                .collect(),
            output: PieceOutput {
                pos: {
                    let pos = self.output.pos;
                    Pos(pos.2 * -1, pos.1, pos.0)
                },
                dir: self.output.dir.rotate_ccw(),
                signal: self.output.signal,
                delay: self.output.delay,
            },
            wire_mask: (self.wire_mask.iter())
                .map(|Pos(x, y, z)| Pos(z * -1, *y, *x))
                .collect(),
            solid_mask: (self.solid_mask.iter())
                .map(|Pos(x, y, z)| Pos(z * -1, *y, *x))
                .collect(),
        }
    }

    pub fn rotate_cw(&self) -> RoutePiece {
        // This is a super lazy way of doing this, but ideally we don't do it
        // too often so it's probably fine.
        self.rotate_ccw().rotate_ccw().rotate_ccw()
    }

    pub fn rotate_n(&self, n: u8) -> RoutePiece {
        match n % 4 {
            0 => self.clone(),
            1 => self.rotate_ccw(),
            2 => self.rotate_ccw().rotate_ccw(),
            3 => self.rotate_cw(),
            _ => unreachable!(),
        }
    }

    pub fn is_straight(&self) -> bool {
        let outpos = self.output.pos;
        outpos.0 == 0 && outpos.2 == 0
    }

    pub fn is_flat(&self) -> bool {
        self.output.pos.1 == 0
    }

    pub fn is_non_rotating(&self) -> bool {
        self.output.dir == Cardinal::East
    }

    pub fn no_delay(&self) -> bool {
        self.output.delay == 0
    }

    pub fn get_expected_signal(&self, s: u8) -> u8 {
        match self.output.signal {
            SignalRule::Absolute(v) => v,
            SignalRule::Relative(r) => match s as i8 + r {
                n if n >= 0 => n as u8,
                n if n < 0 => 0,
                _ => unreachable!(),
            },
        }
    }
}

/// An expected signal strength (for a piece output or something).
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum SignalRule {
    /// The signal will always be this value, as if there was a repeater along
    /// the path that reset the signal drop.
    Absolute(u8),

    /// Relative to some (context-dependent) input, this is how much the signal
    /// should change, usually a negative.
    Relative(i8),
}

// Include a small library of pieces that are known to be good.
include!("base_pieces.rs");
