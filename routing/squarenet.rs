#![allow(unused)]

use std::ops::Range;

use reddl_grid::space;
use reddl_grid::tile;

/*
 * Coordinates used in this module are in circuit space unless otherwise noted,
 * since it simplifies a lot of things for routing.  It converts to block space
 * later when it converts it to blocks.
 */

/// A range type that can have unbounded regions.
#[derive(Copy, Clone, Hash, Debug)]
pub enum MultiRange {
    /// min value to max value, inclusive
    Finite(i32, i32),
    /// infinity to end value
    Max(i32),
    /// start value to infinity
    Min(i32),
    /// infinite to infinite
    Infinite,
    /// nothing
    Empty,
}

impl MultiRange {
    /// Checks to see if the range contains the specified value.
    fn contains(&self, v: i32) -> bool {
        use self::MultiRange::*;
        match self {
            Finite(i, j) => v >= *i && v <= *j,
            Max(x) => v <= *x,
            Min(x) => v >= *x,
            Infinite => true,
            Empty => false,
        }
    }

    /// If the value is a finite, return a pair of ranges for the values not
    /// included in the range.
    fn to_finite_invert(&self) -> Option<(MultiRange, MultiRange)> {
        use self::MultiRange::*;
        match self {
            Finite(i, j) => Some((Max(i - 1), Min(j + 1))),
            _ => None,
        }
    }

    /// If the value is an inifinite (or empty), returns the opposite of the range.
    fn to_infinite_invert(&self) -> Option<MultiRange> {
        use self::MultiRange::*;
        match self {
            Min(x) => Some(Max(x + 1)),
            Max(x) => Some(Min(x - 1)),
            Empty => Some(Infinite),
            Infinite => Some(Empty),
            _ => None,
        }
    }

    /// Returns a range that's extended to include the value specified, but
    /// tries to maintain the same general shape.
    fn extend_to_include(self, v: i32) -> MultiRange {
        use self::MultiRange::*;
        if self.contains(v) {
            return self;
        }

        match self {
            Finite(i, j) => {
                if v < i {
                    Finite(v, j)
                } else {
                    Finite(i, v)
                }
            }
            Max(x) => Max(v),
            Min(x) => Min(v),
            Infinite => Infinite,
            Empty => Finite(v, v),
        }
    }
}

/// A pin coming out of a step.
#[derive(Clone)]
struct OutPin {
    pos: i32,
}

/// A pin going into a step.
#[derive(Clone)]
struct InPin {
    pos: i32,
}

/// A link going up to the cross layer connecting an input
#[derive(Clone)]
struct CrossLink {
    x_pos: i32,
    source: i32,
    dests: Vec<i32>,
}

/// A wire on a track.
#[derive(Clone)]
struct TrackLink {
    track_idx: i32,
    range: MultiRange,
}

impl CrossLink {
    /// Extents is an inclusive range.
    fn extents(&self) -> (i32, i32) {
        let mut min = self.source;
        let mut max = self.source;
        for dt in &self.dests {
            if *dt < min {
                min = *dt;
            }
            if *dt > max {
                max = *dt;
            }
        }
        (min, max)
    }

    /// Checks to see if there is a conflict with this cross link.
    fn conflicts(&self, track: i32) -> bool {
        let (min, max) = self.extents();
        track >= min && track <= max
    }

    fn to_range(&self) -> MultiRange {
        let (i, j) = self.extents();
        MultiRange::Finite(i, j)
    }
}

#[derive(Clone)]
struct ChannelRoute {
    start: OutPin,
    end: InPin,
}

#[derive(Clone)]
struct ChannelSpec {
    routes: Vec<ChannelRoute>,
}

#[derive(Clone)]
struct ChannelLayout {
    track_links: Vec<TrackLink>,
    cross_links: Vec<CrossLink>,
}

enum RouteStep {
    Track(TrackLink),
    Cross(CrossLink),
}

impl ChannelLayout {
    fn is_point_clear(&self, (x, z): (i32, i32)) -> bool {
        // TODO Make this faster with some hashtable or something.

        for tl in &self.track_links {
            if tl.track_idx != z {
                continue;
            }
            if tl.range.contains(x) {
                return false;
            }
        }
        for cl in &self.cross_links {
            if cl.x_pos != x {
                continue;
            }
            if cl.conflicts(x) {
                return false;
            }
        }
        return true;
    }

    // Returns the (x, z) finite ranges of the solution.
    fn to_bounding_box(&self) -> (MultiRange, MultiRange) {
        use self::MultiRange::*;
        let mut xrange = MultiRange::Empty;
        let mut zrange = MultiRange::Empty;

        for tl in &self.track_links {
            xrange = xrange.extend_to_include(tl.track_idx);
            match tl.range {
                Finite(i, j) => {
                    zrange = zrange.extend_to_include(i).extend_to_include(j);
                }
                Min(x) => zrange = zrange.extend_to_include(x + 1),
                Max(x) => {
                    zrange = zrange.extend_to_include(x - 1);
                }
                _ => {} // idk about this yet
            }
        }

        for cl in &self.cross_links {
            xrange = xrange.extend_to_include(cl.x_pos);
            let (zmin, zmax) = cl.extents();
            zrange.extend_to_include(zmin).extend_to_include(zmax);
        }

        (xrange, zrange)
    }
}

fn compute_route(layout: &ChannelLayout, route: &ChannelRoute) -> Vec<RouteStep> {
    use self::MultiRange::*;

    // Figure out bounding boxes.
    let (xb, zb) = layout.to_bounding_box();
    let (xmin, xmax) = match xb {
        Finite(i, j) => (i, j),
        Empty => (0, 0),
        _ => panic!("unknown x bounds"),
    };
    let (zmin, zmax) = match zb {
        Finite(i, j) => (i, j),
        Empty => (0, 0),
        _ => panic!("unknown z bounds"),
    };

    // This is how much we have to move over.
    let track_z_dist = route.end.pos - route.start.pos;

    // Route steps so far.
    let mut steps: Vec<RouteStep> = Vec::new();

    // cz is basically the track id
    let mut cz = route.start.pos;
    let mut cx = xmin; // I think?

    // something...
    while !check_track_free_to_end(layout, cz, cx) {
        unimplemented!()
    }

    if steps.len() > 0 {
        // Put in the last step to the end of the track.
        steps.push(RouteStep::Track(TrackLink {
            track_idx: cz,
            range: MultiRange::Min(cx),
        }));
    } else {
        // Put in the only, inifinite track link.
        steps.push(RouteStep::Track(TrackLink {
            track_idx: cz,
            range: MultiRange::Infinite,
        }));
    }

    // Return
    steps
}

fn check_route_steps(layout: &ChannelLayout, steps: &Vec<RouteStep>) -> bool {
    use self::MultiRange::*;
    let (xb, zb) = layout.to_bounding_box();

    // TODO
    false
}

fn check_track_free_to_end(layout: &ChannelLayout, track: i32, x: i32) -> bool {
    use self::MultiRange::*;
    let (xb, _) = layout.to_bounding_box();

    let bb_end = match xb {
        f @ Finite(_, _) => match f.to_finite_invert() {
            Some((_, Min(y))) => y,
            _ => panic!("this didn't work wtf"),
        },
        Empty => return true,
        _ => panic!("aahhhhhh!"),
    };

    for tl in &layout.track_links {
        if tl.track_idx != track {
            continue;
        }
    }

    true
}

/// Finds a set of x coords where we could put a cross link, with the first and
/// last being able to move as far as needed back or forwards, respectively.
fn find_free_cross_link(layout: &ChannelLayout, start_z: i32, end_z: i32) -> Vec<i32> {
    use self::MultiRange::*;
    let (xb, _) = layout.to_bounding_box();
    match xb {
        Finite(min, max) => {
            let mut slices = Vec::new();

            // Put on beyond the near bounds.
            slices.push(min - 1);

            // Now check all the inner x values.
            for x in min..=max {
                'l_iter: for l in &layout.cross_links {
                    // If it's not the layer then forget about it.
                    // this way of doing things is inefficient, make it faster
                    if l.x_pos != x {
                        continue 'l_iter;
                    }

                    // Now go through all of our Zs and make sure that we
                    // don't conflict.
                    for z in start_z..=end_z {
                        if l.to_range().contains(z) {
                            continue 'l_iter;
                        }
                    }

                    // We've checked what we need to, now add it to the list of ok ones.
                    slices.push(l.x_pos);
                }
            }

            // Now put on the last one outside the far bounds.
            slices.push(max + 1);

            // Return
            slices
        }
        Empty => vec![0, 0],
        _ => panic!("ahhhh"),
    }
}

/*
TODO Make this work later.

/// In cases where there's multiple destination pins we should just find the
/// farthest empty set of tracks that can connect.
fn find_multipin_x(layout: &ChannelLayout, dests: &Vec<i32>) -> i32 {
    let (xb, _) = layout.to_bounding_box();
    let mut x = 0; // Ideally we should start in the "middle" and only go fwd.
    'xiter: loop {
        for d in dests {
            let mut xt = x;
            while xb.contains(xt) {
                if !layout.is_point_clear((xt, *d)) {
                    continue 'xiter;
                }
            }
        }
        break;
    }
    x
}
 */

#[derive(Clone)]
enum StepOption {
    /// Range, upwards to infinity.
    CrossPosInfinite(i32),

    /// Range, downwards to infinity.
    CrossNegInfinite(i32),

    /// Once z position.
    CrossPoint(i32),

    /// Min value, off to infinity.
    TrackInfinite(i32),

    /// A single x position.
    TrackPoint(i32),
}

fn compute_available_connections(layout: &ChannelLayout, (x, z): (i32, i32)) -> Vec<StepOption> {
    let mut conns = Vec::new();
    let (xb, zb) = layout.to_bounding_box();

    // Trace forward until we're beyond anything else.
    let mut xp = x;
    while xb.contains(xp) {
        if layout.is_point_clear((xp, z)) {
            conns.push(StepOption::TrackPoint(xp))
        }
        xp += 1;
    }
    conns.push(StepOption::TrackInfinite(xp));

    // Check increasing z points.
    let mut zpp = z;
    while zb.contains(zpp) {
        if layout.is_point_clear((x, zpp)) {
            conns.push(StepOption::CrossPoint(zpp));
        }
        zpp += 1;
    }
    conns.push(StepOption::CrossPosInfinite(zpp));

    // Check decreasing z points.
    let mut znp = z - 1;
    while zb.contains(znp) {
        if layout.is_point_clear((x, znp)) {
            conns.push(StepOption::CrossPoint(znp))
        }
        znp -= 1;
    }
    conns.push(StepOption::CrossNegInfinite(znp));

    conns
}
