pub fn get_base_pieces() -> Vec<RoutePiece> {
    use self::SignalRule::*;
    use reddl_grid::tile::Cardinal::*;
    use reddl_grid::tile::Tile::*;

    // Just a single piece.
    let one_step = RoutePiece {
        tiles: vec![(Pos(0, 0, 0), Wire), (Pos(0, -1, 0), Solid)],
        output: PieceOutput {
            pos: Pos(1, 0, 0),
            dir: East,
            signal: Relative(-1),
            delay: 0,
        },
        wire_mask: vec![Pos(0, 0, 1), Pos(0, 0, -1), Pos(0, -1, 1), Pos(0, 1, -1)],
        solid_mask: vec![], // I think this is ok.
    };

    // Move down 2, moving forward.
    let up_2 = RoutePiece {
        tiles: vec![
            (Pos(0, 0, 0), Wire),
            (Pos(0, -1, 0), Solid),
            (Pos(1, 1, 0), Wire),
            (Pos(1, 0, 0), Solid),
            (Pos(2, 2, 0), Wire),
            (Pos(2, 1, 0), Solid),
        ],
        output: PieceOutput {
            pos: Pos(3, 2, 0),
            dir: East,
            signal: Relative(-3),
            delay: 0,
        },
        wire_mask: vec![],
        solid_mask: vec![], // TODO
    };

    // Move down 2, moving forward.
    let down_2 = RoutePiece {
        tiles: vec![
            (Pos(0, 0, 0), Wire),
            (Pos(0, -1, 0), Solid),
            (Pos(1, -1, 0), Wire),
            (Pos(1, -2, 0), Solid),
            (Pos(2, -2, 0), Wire),
            (Pos(2, -3, 0), Solid),
        ],
        output: PieceOutput {
            pos: Pos(3, -2, 0),
            dir: East,
            signal: Relative(-3),
            delay: 0,
        },
        wire_mask: vec![],  // TODO
        solid_mask: vec![], // TODO
    };

    // Just a repeater, resets signal to 15.
    let repeater_one_step = RoutePiece {
        tiles: vec![
            (Pos(0, 0, 0), Repeater(Cardinal::East, 1)),
            (Pos(0, -1, 0), Solid),
        ],
        output: PieceOutput {
            pos: Pos(1, 0, 0),
            dir: East,
            signal: Absolute(15),
            delay: 2,
        },
        wire_mask: vec![],  // this is actually correct
        solid_mask: vec![], // I think this is too
    };

    // Turns left using 3 block in a short L shape.
    let turn_left = RoutePiece {
        tiles: vec![
            (Pos(0, 0, 0), Wire),
            (Pos(1, 0, 0), Wire),
            (Pos(1, 0, -1), Wire),
            (Pos(0, -1, 0), Solid),
            (Pos(1, -1, 0), Solid),
            (Pos(1, -1, -1), Solid),
        ],
        output: PieceOutput {
            pos: Pos(1, 0, -2),
            dir: North,
            signal: Relative(-3),
            delay: 0,
        },
        wire_mask: vec![],  // TODO
        solid_mask: vec![], // TODO
    };

    // Turns right using 3 blocks in an L shape.
    let turn_right = RoutePiece {
        tiles: vec![
            (Pos(0, 0, 0), Wire),
            (Pos(1, 0, 0), Wire),
            (Pos(1, 0, 1), Wire),
            (Pos(0, -1, 0), Solid),
            (Pos(1, -1, 0), Solid),
            (Pos(1, -1, 1), Solid),
        ],
        output: PieceOutput {
            pos: Pos(1, 0, 2),
            dir: South,
            signal: Relative(-3),
            delay: 0,
        },
        wire_mask: vec![],  // TODO
        solid_mask: vec![], // TODO
    };

    /* ================ */

    // Combine them into a vec.
    vec![
        one_step,
        up_2,
        down_2,
        repeater_one_step,
        turn_left,
        turn_right,
    ]
}
