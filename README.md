# RedDL

RedDL (or just "reddl") is a DSL for describing redstone circuits.  There's
been projects for this purpose before, but this does it better and is designed
for practical use.

Still a work-in-progress.  Not much interesting to see yet.

The only really interesting stuff to see is in the `bus` crate.  The `routing`
crate has a whole bunch of in-progress stuff, it's for connecting two steps in
a pipeline, and then later for bridging inputs/outputs of a pipeline onto a bus
connector so it can be used.

The `grid` crate is a bunch of utilities for working with tiles (blocks), like
for storing world data.  There's a nice data structure called `Space<T>` in
there for storing large regions of unknown size efficiently.  It still needs a
little work because I don't have a good way of representing things like pistons
and comparators and locked repeater states and stuff, but it does the basics for
now.
