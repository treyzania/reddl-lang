#![allow(unused)]

use std::io::{self, Write};

use reddl_grid::space;
use reddl_grid::tile;
use reddl_grid::Pos;

type StructureSpace = space::Space<tile::Tile>;

#[derive(Clone, Hash, Debug)]
pub struct CharMapping {
    sym: char,
    block_state: String,
}

/// Describes a point in 3D space, using i32s.  Or alternatively, bounds.
#[derive(Clone)]
struct Point3 {
    x: i32,
    y: i32,
    z: i32,
}

/// This is the actual structure representation, ready to be serialized.  I
/// don't really like this name but we'll figure that out later.  SSRF looks
/// werid when you write it as "Ssrf".
#[derive(Clone)]
pub struct StructureRep {
    bounds: Point3,
    zero_offset: Point3,
    mappings: Vec<CharMapping>,
    layers: Box<[Layer]>,
}

#[derive(Clone)]
struct Layer {
    tiles: Box<[Box<[char]>]>,
}

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Unknown(String),
}

impl From<io::Error> for Error {
    fn from(f: io::Error) -> Self {
        Error::Io(f)
    }
}

impl StructureRep {
    fn from_space(s: StructureSpace) -> Result<StructureRep, Error> {
        unimplemented!()
    }

    fn serialize<W: Write>(&self, dest: &mut W) -> Result<(), Error> {
        let bounds_line = format!(
            "bounds {} {} {}\n",
            self.bounds.x, self.bounds.y, self.bounds.z
        );
        let zo_line = format!(
            "zerooffset {} {} {}\n",
            self.zero_offset.x, self.zero_offset.y, self.zero_offset.z
        );

        // First write the bounds and the zero offset thing.
        dest.write_all(bounds_line.as_bytes())?;
        dest.write_all(zo_line.as_bytes())?;

        // Write in the character mappings.
        for m in &self.mappings {
            let l = format!("charid {} '{}'\n", m.sym, m.block_state);
            dest.write_all(l.as_bytes())?;
        }

        // Now a separator for each layer.
        dest.write_all("\n".as_bytes())?;

        // Now do the layers, which takes a while.
        for l in self.layers.as_ref() {
            for row in l.tiles.as_ref() {
                // FIXME this does more copying than it should need to
                let rl = format!("{}\n", row.as_ref().iter().collect::<String>());
                dest.write_all(rl.as_bytes());
            }

            // We separate layers with a blank line.
            dest.write_all("\n".as_bytes())?;
        }

        Ok(())
    }
}
