# reddl-ssrf

So SSRF is a "simple structure representation format".  It serves a similar
purpose as the assembly grammar format, except it's used as an output from our
compiler.

There isn't a schematic format library for Rust, but there is one
[for Python](https://github.com/mcedit/mcedit2/blob/master/src/mceditlib/schematic.py),
and this format is supposed to also be simple to parse in Python, so I'm going
to be writing something to convert SSRF to actual `.schematic` files.

It's probably also going to have some features for resolving minor details about
how to output to schematics, but the format itself does explain which block
types and stuff to put where, I think.

## Pipeline

```
+------------+              +-------+              +------------+
| .rla, .rll | -> reddlc -> | .ssrf | -> ssrfas -> | .schematic |
+------------+              +-------+              +------------+
```
