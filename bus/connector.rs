#![allow(unused)]

/// A pin in a connector.  This refers to the conductor, so like the redstone or
/// the repeaters themselves. It assumes a solid block will be placed beneath.
#[derive(Clone, Hash, Debug)]
pub struct ConnectorPin {
    /// The name of this pin, in addition to the index in its spec.
    name: String,

    /// The position of the conductor.  This should be level 15 power.
    pos: (i32, i32),
}

/// A connector to a bus, the first pin should be at (0, 0).
#[derive(Clone, Hash, Debug)]
pub struct ConnectorSpec {
    /// The name of this connector type.
    name: String,

    /// The pins themselves, index is important!
    pins: Vec<ConnectorPin>,
}

impl ConnectorSpec {
    fn new(name: String, pins: Vec<ConnectorPin>) -> ConnectorSpec {
        ConnectorSpec {
            name: name,
            pins: pins,
        }
    }

    fn merge(
        name: String,
        a: &ConnectorSpec,
        b: &ConnectorSpec,
        (offx, offy): (i32, i32),
    ) -> ConnectorSpec {
        ConnectorSpec {
            name: name,
            pins: a
                .pins
                .iter()
                .cloned()
                .map(|ConnectorPin { name, pos }| ConnectorPin {
                    name: format!("{}-{}", a.name, name),
                    pos: pos,
                })
                .chain(
                    b.pins
                        .iter()
                        .map(|ConnectorPin { name, pos }| ConnectorPin {
                            name: format!("{}-{}", b.name, name),
                            pos: (pos.0 + offx, pos.1 + offy),
                        }),
                )
                .collect(),
        }
    }
}
