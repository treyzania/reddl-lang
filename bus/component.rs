use reddl_grid::space::*;
use reddl_grid::tile::*;
use reddl_grid::Pos;

use crate::connector;

#[derive(Clone)]
pub enum ConnectorMode {
    Input,
    Output,
}

/// A connector on a bus, in a particular orientation.
#[derive(Clone)]
struct BusConnector {
    spec: connector::ConnectorSpec, // maybe make this an Arc?

    /// The position within the component of the origin of the bus.
    orig: Pos,

    /// Which orientation this connector is facing, outwards.
    orientation: Cardinal,

    /// Is this an input or an output?
    mode: ConnectorMode,
}

/// A definition for a particular bus component, with connectors to bus lines.
#[derive(Clone)]
pub struct ComponentSpec {
    /// The external connectors for this component, not counting composite
    /// connectors.
    base_connectors: Vec<(String, BusConnector)>,

    /// The tiles that make up this bus.
    tiles: Space<Tile>,
}
