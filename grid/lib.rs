#![allow(unused)]
#![feature(euclidean_division)]

use std::collections::*;
use std::ops::*;

pub mod space;
pub mod tile;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct Pos(pub i32, pub i32, pub i32);

impl Add for Pos {
    type Output = Pos;
    fn add(self, o: Pos) -> Self::Output {
        Pos(self.0 + o.0, self.1 + o.1, self.2 + o.2)
    }
}

impl Pos {
    pub fn rotate_ccw_around_origin(self) -> Pos {
        Pos(self.2 * -1, self.1, self.0)
    }
    pub fn rotate_n_around_origin(self, n: u8) -> Pos {
        // TODO Make this better, this is awful lol.
        match n % 4 {
            0 => self,
            1 => self.rotate_ccw_around_origin(),
            2 => self.rotate_ccw_around_origin().rotate_ccw_around_origin(),
            3 => self
                .rotate_ccw_around_origin()
                .rotate_ccw_around_origin()
                .rotate_ccw_around_origin(),
            _ => unreachable!(),
        }
    }
}
