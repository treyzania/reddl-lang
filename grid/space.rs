use crate::*;

type ChunkPos = (i32, i32, i32);
type SubchunkPos = (u8, u8, u8);

/// Meta-trait for things we can put in a grid.
pub trait SpaceCell: Default + Clone + Copy {}

#[derive(Clone)]
pub struct Space<T: SpaceCell> {
    chunks: HashMap<ChunkPos, Chunk<T>>,
}

impl<T: SpaceCell> Space<T> {
    pub fn new() -> Space<T> {
        Space {
            chunks: HashMap::new(),
        }
    }

    pub fn get_tile_at(&self, pos: Pos) -> T {
        let (cp, scp) = pos_destruct(pos);
        self.chunks
            .get(&cp)
            .map(|c| c.get_tile_at(scp))
            .unwrap_or(T::default())
    }

    pub fn set_tile_at(&mut self, pos: Pos, tile: T) {
        let (cp, scp) = pos_destruct(pos);
        let c = self.mut_chunk(cp);
        c.set_tile_at(scp, tile);
    }

    fn mut_chunk(&mut self, cpos: ChunkPos) -> &mut Chunk<T> {
        self.ensure_chunk_exists(cpos);
        self.chunks.get_mut(&cpos).unwrap()
    }

    fn ensure_chunk_exists(&mut self, cpos: ChunkPos) {
        if !self.chunks.contains_key(&cpos) {
            self.chunks.insert(cpos, Chunk::new_empty());
        }
    }
}

const CHUNK_SIZE: usize = 16 * 16 * 16;

#[derive(Clone)]
pub struct Chunk<T: SpaceCell> {
    tiles: [T; CHUNK_SIZE],
}

impl<T: SpaceCell> Chunk<T> {
    fn new_empty() -> Chunk<T> {
        Chunk {
            tiles: [<T as Default>::default(); CHUNK_SIZE],
        }
    }

    fn get_tile_at(&self, pos: SubchunkPos) -> T {
        self.tiles[subchunk_pos_to_index(pos)].clone()
    }

    fn set_tile_at(&mut self, pos: SubchunkPos, tile: T) {
        self.tiles[subchunk_pos_to_index(pos)] = tile;
    }
}

#[inline(always)]
fn subchunk_pos_to_index((lx, ly, lz): SubchunkPos) -> usize {
    (((lx as usize * CHUNK_SIZE) + ly as usize) * CHUNK_SIZE) + lz as usize
}

#[inline(always)]
fn pos_destruct(Pos(gx, gy, gz): Pos) -> (ChunkPos, SubchunkPos) {
    // Find the local subchunk coords.
    let lx = gx.mod_euc(CHUNK_SIZE as i32);
    let ly = gy.mod_euc(CHUNK_SIZE as i32);
    let lz = gz.mod_euc(CHUNK_SIZE as i32);
    let scp = (lx as u8, ly as u8, lz as u8);

    // Now the global chunk coords.
    let cx = gx.div_euc(CHUNK_SIZE as i32);
    let cy = gy.div_euc(CHUNK_SIZE as i32);
    let cz = gz.div_euc(CHUNK_SIZE as i32);
    let cp = (cx, cy, cz);

    // And that's is.
    (cp, scp)
}

#[inline(always)]
fn pos_restruct((cx, cy, cz): ChunkPos, (lx, ly, lz): SubchunkPos) -> Pos {
    Pos(
        cx * CHUNK_SIZE as i32 + lx as i32,
        cy * CHUNK_SIZE as i32 + ly as i32,
        cz * CHUNK_SIZE as i32 + lz as i32,
    )
}
