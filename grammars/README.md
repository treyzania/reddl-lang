# reddl-grammars

RedDL has two main grammars that I've planned out so far.

## Assembly (`.rla`)

So you're building an adder.  How does it work?  This is the language you use
to build that.  It's pretty simple and looks a little awkward since we're
basically doing stuff like laying out blocks, a complex grammar doesn't really
work well here.

## Layout (`.rll`)

This is for describing things like the layout of components and busses and more
complex pieces of things, including specs for connectors and ROMs.

## Rom? (`.rlr`?)

Not sure if I'll actually have a particular format for this.  But it's what's
used for defining data used in a ROM, like microcode.  It might be represented
as hex or plaintext binary or something.

Maybe there will be special format for different banks or leaving sections of
ROM uninitialized or something.

(ROMs are probably going to have multiple forms of representation, like lookup
tables, piston tapes, etc.)
