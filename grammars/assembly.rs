#![allow(unused)]

/*
 * Types in this file that start with "Asm" are not an output data
 * representation format, they're for internal use when figuring out how to
 * parse the different sections of the assembly files.
 *
 * And by "assembly" I mean the mechanical kind not the programming kind.
 */

use std::io::BufRead;

#[derive(Clone)]
pub enum Error {
    UnexpectedToken(String),
    InvalidSectionHeader(String),
    LineNotInHeader(String),
    ReadError, // TODO more
}

/// Structural preprocessing format, not rich data yet.  Each section has own
/// parse rules.
#[derive(Clone)]
struct AsmSection {
    header: SectionHeader,
    lines: Vec<String>,
}

#[derive(Clone)]
struct SectionHeader {
    label: String,
    flags: Option<Vec<String>>,
}

/// Returns a section header parsed, like `[foo flag]`.
fn parse_section_header(s: &String) -> Result<SectionHeader, Error> {
    let len = s.len();
    let first = s.chars().nth(0).unwrap();
    let last = s.chars().last().unwrap();
    if first == '[' && last == ']' {
        let mut inner: String = s.chars().skip(1).take(len - 2).collect();
        inner.trim();

        if inner.contains(" ") {
            // This isn't super efficient but it doesn't need to.
            let parts: Vec<String> = inner.split(" ").map(String::from).collect();
            let label = parts[0].clone();
            let flags = Some(parts[1..].iter().cloned().collect());
            Ok(SectionHeader { label, flags })
        } else {
            Ok(SectionHeader {
                label: inner,
                flags: None,
            })
        }
    } else {
        Err(Error::InvalidSectionHeader(s.clone()))
    }
}

fn parse_assembly_sections<R: BufRead>(src: &mut R) -> Result<Vec<AsmSection>, Error> {
    let mut finished_sections = Vec::new();

    let mut sec: Option<AsmSection> = None;

    for lineres in src.lines() {
        let line = match lineres {
            Ok(l) => l,
            Err(e) => return Err(Error::ReadError),
        };

        let hdr = parse_section_header(&line);
        match hdr {
            Ok(h) => match sec {
                Some(s) => {
                    finished_sections.push(s.clone());
                    sec = Some(AsmSection {
                        header: h,
                        lines: Vec::new(),
                    });
                }
                None => {
                    sec = Some(AsmSection {
                        header: h,
                        lines: Vec::new(),
                    })
                }
            },
            Err(_) => match sec {
                Some(ref mut s) => s.lines.push(line.clone()),
                None => return Err(Error::LineNotInHeader(line.clone())),
            },
        }
    }

    // Push the last one on if there is one.
    if sec.is_some() {
        finished_sections.push(sec.unwrap());
    }

    Ok(finished_sections)
}

/// Ordering is fun.  Each character is one unit of the first element, each
/// newline is one unit of the second element (if prefixed by an n, it's still
/// 1 but the coords are flipped), see below, and double newlines are one unit
/// in the third element.
#[derive(Clone)]
enum Ordering {
    /// Flat layers, each char goes east, newlines go south, each block is
    /// another layer upwards.
    Xzy,

    /// Flat layers, each char goes south, newlines go down, each block is
    /// another layer east.
    Znyx,
}

impl Ordering {
    fn from_flag(s: &str) -> Option<Ordering> {
        match s {
            "xzy" => Some(Ordering::Xzy),
            "znyx" => Some(Ordering::Znyx),
            _ => None,
        }
    }
}

#[derive(Clone)]
pub struct AssemblyDef {
    header: AssemblyHeader,
    dict: Vec<DictEntry>,
    // TODO Tiles.
}

#[derive(Clone)]
struct AssemblyHeader {
    name: String,
}

pub type TileType = String;

#[derive(Clone, Hash, Debug)]
pub enum DictEntry {
    Symbol(TileType, char),
}

impl DictEntry {
    fn from_line(ss: Vec<&str>) -> Result<DictEntry, Error> {
        match ss[0] {
            "symbol" => {
                let tt = String::from(ss[1]);
                let c = ss[2].chars().nth(0).unwrap();
                Ok(DictEntry::Symbol(tt, c))
            }
            c => Err(Error::UnexpectedToken(String::from(c))),
        }
    }
}

fn check_string_starts_with(s: &String, starts: &str) -> bool {
    // this is kinda messy, hopefully it works?
    s.chars()
        .zip(starts.chars())
        .fold(true, |p, (a, b)| p && (a == b))
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_string_starts_with() {
        assert!(check_string_starts_with(&"hello, world".into(), "hello"));
    }

    #[test]
    fn test_string_starts_with_false() {
        assert!(!check_string_starts_with(&"something".into(), "nope"));
    }

}
