extern crate reddl_grid;

// The assembly module is for definining discrete components, with individual
// tiles and stuff.
pub mod assembly;

// Layout files are for defining instances of assemblies and defining how
// they're connected together into more complex components.
pub mod layout;
